import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.lang.IndexOutOfBoundsException;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Una clase para la ejecución del Videojuego.
 *
 * @author Yare Pérez
 * @version 1.0, 10/27/22
 * @since 1.0
 *
 */
public class Main {
    /**
     * Constructor por defecto.
     * @since 1.0
     *
     */
    public Main() {}

    /**
     * El método por defecto usado para inicializar la ejecución del videojuego.
     *
     * @param args un arreglo de Strings con los argumentos proporcionados.
     * @exception IOException Si se produce un error de E/S.
     * @since 1.0
     *
     */
    public static void main(String[] args) throws IOException {
        int opcion = 0;
        Menu menu = new Menu();
        Scanner sc = new Scanner(System.in);
        Videojuego videojuego = new Videojuego();

        do {
            menu.mostrar();
            System.out.print("Teclea una opción: ");
            opcion = sc.nextInt();
            switch (opcion) {
                case 1:
                    videojuego.jugar();
                    break;
                case 2:
                    menu.mostrarInstrucciones();
                    break;
                case 3:
                    menu.mostrarPuntuaciones();
                    break;
                case 4:
                    System.out.println("¡Gracias por jugar!");
                    break;
                default:
                    System.out.println("¡Por favor elija una opción válida!");
            }

        } while (opcion != 4);
    }

}
