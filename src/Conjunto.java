import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.lang.IndexOutOfBoundsException;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Esta clase proporciona la funcionalidad adecuada para el manejo de un conjunto de números enteros.
 * El objetivo principal es minimizar el esfuerzo al manipular un conjunto de números aleatorios.
 *
 * @author Yare Pérez
 * @version 1.0, 10/27/22
 * @since 1.0
 *
 */
public class Conjunto {
    /**
     * Estos números se ordenan utilizando su ordenamiento natural para números enteros.
     *
     * @see #getNumeros()
     */
    private TreeSet<Integer> numeros;

    /**
     * Constructor por defecto.
     * El objetivo de este constructor es inicializar correctamentes los atributos de la clase.
     * @since 1.0
     *
     */
    public Conjunto() {
        numeros = new TreeSet<>();
    }

    /**
     * Este método obtiene los números presentes en este conjunto.
     *
     * @return un conjunto de numeros enteros.
     * @since 1.0
     *
     */
    public TreeSet<Integer> getNumeros(){
        return numeros;
    }

    /**
     * Este método reemplaza los números presentes en este conjunto.
     *
     * @param numeros un conjunto de numeros enteros.
     * @since 1.0
     *
     */
    public void setNumeros(TreeSet<Integer> numeros){
        this.numeros = numeros;
    }
}
