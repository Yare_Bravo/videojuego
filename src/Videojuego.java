import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.lang.IndexOutOfBoundsException;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Una clase para el Videojuego.
 *
 * @author Yare Pérez
 * @version 1.0, 10/27/22
 * @since 1.0
 *
 */
public class Videojuego {
    /**
     * Nombre del archivo de punuaciones.
     *
     */
    public static final String ARCHIVO_PUNTUACIONES = "puntuaciones.txt";

    /**
     * Constructor por defecto.
     * @since 1.0
     * @exception IOException Si se produce un error de E/S.
     *
     */
    public Videojuego() throws IOException{
        FileWriter escritor = new FileWriter​(ARCHIVO_PUNTUACIONES, true);
        escritor.write("", 0, 0);
        escritor.flush();
    }

    /**
     * Una vez calculado el puntaje el programa debe preguntar al usuario su nombre para que,
     * acto seguido, escriba en un archivo una línea con el nombre y el puntaje
     * (se debe añadir esa línea manteniendo el contenido anterior del archivo de puntuaciones).
     * @since 1.0
     * @param puntaje puntaje de los numeros atinados.
     * @exception IOException Si se produce un error de E/S.
     *
    */
    public void guardarPuntaje(int puntaje) throws IOException {
        FileWriter escritor = new FileWriter​(ARCHIVO_PUNTUACIONES, true);
        Scanner sc = new Scanner(System.in);

        System.out.println("Introduce tu nombre");
        String nombre = sc.nextLine();
        String registroDePuntuacion = nombre + " " + puntaje + "\n";
        escritor.write(registroDePuntuacion, 0, registroDePuntuacion.length());
        escritor.flush();
    }

   /**
     * Este método genera un conjunto al azar de numeros enteros entre 1 y 100, sin repeticiones.
     *
     * @param tamaño el tamaño del conjunto a generar.
     * @return un conjunto de numeros aleatorios.
     * @since 1.0
     *
     */
    public Conjunto generarConjunto(int tamaño) {
        // genera al azar un conjunto de números entre el 1 y el 100
        int numero = 0;
        double azar = 0.0;

        TreeSet<Integer> numeros = new TreeSet<>();

        while(numeros.size() < tamaño){
            azar = 1 + (100 * Math.random());
            numero = (int) azar;
            numeros.add(numero);
        }
        Conjunto conjunto = new Conjunto();
        conjunto.setNumeros(numeros);
        return conjunto;
    }

   /**
     * El usuario debe ingresar un conjunto solución sin repeticiones (se recomienda que se le mande una alerta al usuario si intenta ingresar más de una vez un número).
     *
     * @param tamaño el tamaño del conjunto a solicitar.
     * @return un conjunto de numeros.
     * @since 1.0
     *
     */
    public Conjunto solicitarConjunto(int tamaño) {
        int solicitado = 0;
        int numero = 0;
        double azar = 0.0;
        Scanner sc = new Scanner(System.in);
        TreeSet<Integer> numeros = new TreeSet<>();

        while(numeros.size() < tamaño){
            solicitado = numeros.size()+1;
            System.out.print("Ingrese numero "+ solicitado+" :");
            numero = sc.nextInt();
            if (numeros.add(numero) == false) {
                System.out.println("Ya se ha agregado el numero que desea ingresar");
            }
        }
        Conjunto conjunto = new Conjunto();
        conjunto.setNumeros(numeros);
        return conjunto;
    }


   /**
     * Una vez que el usuario ingrese su conjunto solución se deben buscar los elementos del conjunto solución en el conjunto que genero el juego al azar y debe indicar, para cada uno de dichos * * elementos, si ese conjunto está o no está en el conjunto del programa. Deberá mostrar en un mensaje cuantos números atinó el usuario del conjunto solución
     *
     * @param numerosGenerados numeros creados al azar.
     * @param numerosSolicitados numeros ingresados  por el usuario.
     * @return puntaje de los numeros atinados.
     * @since 1.0
     *
     */
    public int calcularPuntuacion (TreeSet<Integer> numerosGenerados, TreeSet<Integer> numerosSolicitados) {
        int puntaje = 0;
        TreeSet<Integer> numerosAtinados = new TreeSet<Integer>(numerosGenerados);
        boolean atinó = numerosAtinados.retainAll(numerosSolicitados);
        puntaje = atinó ? numerosAtinados.size() : 0;

        return puntaje;
    }

    /**
     * Este método tiene como objetivo jugar el videojuego.
     *
     * @exception IOException Si se produce un error de E/S.
     * @since 1.0
     *
     */
    public void jugar() throws IOException{
        String opcion;
        Scanner sc = new Scanner(System.in);
        do {
            this.jugarRonda();
            System.out.print("¿Desea volver a jugar? (Si o No): ");
            opcion = sc.nextLine();
        } while (opcion.compareToIgnoreCase("Si") == 0);
    }


    /**
     * Este método tiene como objetivo jugar una sola ronda del videojuego.
     *
     * @since 1.0
     *
     */
    private void jugarRonda() throws IOException{
        System.out.print("Ingrese tamaño del conjunto: ");
        Scanner sc = new Scanner(System.in);
        int tamaño = sc.nextInt();
        int puntaje = 0;
        Conjunto conjuntoGenerado = this.generarConjunto(tamaño);
        Conjunto conjuntoSolicitado = this.solicitarConjunto(tamaño);
        TreeSet<Integer> numerosGenerados = conjuntoGenerado.getNumeros();
        TreeSet<Integer> numerosSolicitados = conjuntoSolicitado.getNumeros();
        System.out.println("numerosGenerados: " + numerosGenerados);
        System.out.println("numerosSolicitados: " + numerosSolicitados);
        puntaje = this.calcularPuntuacion(numerosGenerados,numerosSolicitados);
        if (puntaje == 0) {
            System.out.println("¡Ups ningún acierto obtuvo!");
        } else {
            System.out.println("¡Felicidades usted atinó a " + puntaje + " números!");
            this.guardarPuntaje(puntaje);
        }
    }
}
