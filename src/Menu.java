import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.lang.IndexOutOfBoundsException;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Un menú con las opciones para jugar, mostrar instrucciones, mostrar puntuaciones y salir.
 *
 * @author Yare Pérez
 * @version 1.0, 10/28/22
 * @since 1.0
 *
 */
public class Menu {
    /**
     * Constructor por defecto.
     * El objetivo de este constructor es inicializar correctamentes los atributos de la clase.
     * @since 1.0
     *
     */
    public Menu() {
    }

    /**
     * Este método tiene como objetivo mostrar las instrucciones del videojuego.
     *
     * @since 1.0
     *
     */
    public void mostrarInstrucciones(){
        System.out.println("Instruciones del videojuego:");
        System.out.println("");
        System.out.println("1. El jugador elige una cantidad del 1 al 20, de números a adivinar.");
        System.out.println("2. El juego generará una serie de números al azar entre 1-100, con base en cantidad proporcionada.");
        System.out.println("3. El jugador ingresará individualmente los números a adivinar.");
        System.out.println("4. El juego validará si adivinó algún número ingresado.");
        System.out.println("5. El juego mostrará un mensaje de cuantos números atinó el jugador.");
        System.out.println("6. En caso de que haya atinado a más de un número, el juego solicitará el nombre del jugador y guardará su puntuación.");
        System.out.println("7. El juego preguntará si desea volver a jugar.");
        System.out.println("8. Si indica que sí, entonces comienza el juego otra vez, si indica que no quiere volver a jugar, lo debe sacar al menú principal.");
        System.out.println("NOTA: Al ingresar los numeros al azar estos no se deben repetir");
    }

    /**
     * Este método tiene como objetivo mostrar las puntuaciones del videojuego.
     *
     * @exception FileNotFoundException Si no se encuentra el archivo de puntuaciones.
     * @since 1.0
     *
     */
    public void mostrarPuntuaciones() throws FileNotFoundException{
        Scanner sc = new Scanner(new File(Videojuego.ARCHIVO_PUNTUACIONES));
        System.out.println("Puntuaciones del videojuego");
        while (sc.hasNextLine()) {
            String  linea = sc.nextLine();
            System.out.println(linea);
      }
    }

    /**
     * Este método tiene como objetivo mostrar el menu principal del videojuego.
     *
     * @since 1.0
     *
     */
    public void mostrar(){
        System.out.println("Menu del videojuego:");
        System.out.println("1. Jugar.");
        System.out.println("2. Mostrar instrucciones.");
        System.out.println("3. Mostrar puntuaciones.");
        System.out.println("4. Salir.");
    }
}
