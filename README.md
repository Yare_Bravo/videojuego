# Videojuego en consola

El programa debe cumplir con lo siguiente:

1. Un menú con las opciones para jugar, mostrar instrucciones, mostrar puntuaciones y salir.
2. El programa genera al azar un conjunto de números entre el 1 y el 100, dicho conjunto no debe tener repeticiones y el usuario debe indicar el tamaño del conjunto; se recomienda que dicho tamaño vaya de 1 a 20 elementos a adivinar.
3. El usuario debe ingresar un conjunto solución sin repeticiones (se recomienda que se le mande una alerta al usuario si intenta ingresar más de una vez un número).
4. Una vez que el usuario ingrese su conjunto solución se deben buscar los elementos del conjunto solución en el conjunto que genero el juego al azar y debe indicar, para cada uno de dichos elementos, si ese conjunto está o no está en el conjunto del programa. Deberá mostrar en un mensaje cuantos números atinó el usuario del conjunto solución.
5. Una vez calculado el puntaje el programa debe preguntar al usuario su nombre para que, acto seguido, escriba en un archivo una línea con el nombre y el puntaje (se debe añadir esa línea manteniendo el contenido anterior del archivo de puntuaciones).
6. Una vez que termine la ronda le debe preguntar al usuario si  desea volver a jugar. Si indica que sí, entonces comienza el juego otra vez, si indica que no quiere volver a jugar, lo debe sacar al menú principal.

## Características de la entrega

- Deberá poder ejecutarse.
- Validaciones en las entradas.
- Código indentado y legible.
- Ocupar lo mejor posible los modificadores de acceso.
- Comentar su código (es decir, indiquen brevemente qué hacen sus clases, métodos y atributos. Para los métodos indiquen con qué operan y qué regresa una vez ejecutado). Les servirá incluso para comunicarse entre miembros de su equipo.
- Tener al menos dos clases creadas por ustedes.
- Algoritmo y diagrama de flujo del método más complejo que tengan (hint: mientras más distribuyan su código tendrán métodos más cortos y más fáciles de escribir en este punto).
